import 'dart:async';

import 'package:bloc_todoapp/data/todo_repository.dart';
import 'package:bloc_todoapp/models/todo.dart';
import 'package:rxdart/rxdart.dart';

class TodoBloc {
  final TodoRepository todoRepository;
  final _todoSubject = BehaviorSubject<List<Todo>>();
  Stream<List<Todo>> get todoStream => _todoSubject.stream;
  List<Todo> todos = [];

  TodoBloc({
    required this.todoRepository,
  });

  Future<void> toggleTodo(Todo todo, {required bool complete}) async {
    await todoRepository
        .toggleTodo(todo, complete: complete)
        .then((value) => _todoSubject.sink.add(value));
  }

  void toggleActive({required bool complete}) {
    final List<Todo> activeTodos = [];
    for (final element in todos) {
      if (element.complete == complete) activeTodos.add(element);
    }
    _todoSubject.sink.add(activeTodos);
  }

  Future<void> toggleAll({required bool complete}) async {
    todos = await todoRepository.toggleAll(complete: complete);
    _todoSubject.sink.add(todos);
  }

  Future<void> fetchTodos() async {
    await todoRepository
        .getTodos()
        .then((value) => _todoSubject.sink.add(value));
  }

  Future<void> addTodo(Todo todo) async {
    todos = await todoRepository.addTodo(todo);
    _todoSubject.sink.add(todos);
  }

  Future<void> removeTodo(Todo todo) async {
    todos = await todoRepository.removeTodo(todo);
    _todoSubject.sink.add(todos);
  }

  Future<void> removeAllCompleted() async {
    todos = await todoRepository.removeAllCompleted();
    _todoSubject.sink.add(todos);
  }

  Future<void> updateTodo(String task, String note, String id) async {
    todos = await todoRepository.updateTodo(task, note, id);
    _todoSubject.sink.add(todos);
  }

  void dispose() {
    _todoSubject.close();
  }
}
