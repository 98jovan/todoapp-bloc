class Todo {
  final String id = DateTime.now().microsecondsSinceEpoch.toString();
  String task;
  bool complete;
  String note;

  Todo({
    required this.task,
    required this.complete,
    required this.note,
  });
}
