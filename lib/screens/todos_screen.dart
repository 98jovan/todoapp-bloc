import 'package:bloc_todoapp/blocs/todo_bloc.dart';
import 'package:bloc_todoapp/models/todo.dart';
import 'package:bloc_todoapp/screens/detail_screen.dart';
import 'package:bloc_todoapp/widgets/filter_popup_menu.dart';
import 'package:bloc_todoapp/widgets/popup_menu_items.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class TodosScreen extends StatelessWidget {
  const TodosScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = context.watch<TodoBloc>();
    return Scaffold(
      appBar: AppBar(
        title: const Text('Rx Example'),
        actions: [
          FilterPopUpMenu(bloc: bloc),
          AppBarPopupmenu(
            bloc: bloc,
          ),
        ],
      ),
      body: StreamBuilder<List<Todo>>(
        stream: bloc.todoStream,
        builder: (context, snapshot) {
          if (snapshot.hasData) {
            final result = snapshot.data;
            if (result!.isEmpty) {
              return const Center(
                child: Text('The list is empty'),
              );
            } else {
              return ListView.builder(
                itemCount: result.length,
                itemBuilder: (context, index) {
                  return Dismissible(
                    key: const ValueKey('key'),
                    onDismissed: (dis) {
                      bloc.removeTodo(result[index]);
                    },
                    child: ListTile(
                      onTap: () {
                        Navigator.of(context).push(
                          MaterialPageRoute(
                            builder: (_) {
                              return DetailScreen(
                                todo: result[index],
                              );
                            },
                          ),
                        );
                      },
                      leading: Checkbox(
                        value: result[index].complete,
                        onChanged: (value) {
                          bloc.toggleTodo(
                            result[index],
                            complete: result[index].complete,
                          );
                        },
                      ),
                      title: Text(
                        result[index].task,
                        style: Theme.of(context).textTheme.headline6,
                      ),
                      subtitle: Text(
                        result[index].note,
                        maxLines: 1,
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(context).textTheme.subtitle1,
                      ),
                    ),
                  );
                },
              );
            }
          } else if (snapshot.hasError) {
            return const Center(
              child: Text('An Error occured'),
            );
          } else {
            return const Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, '/add');
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
