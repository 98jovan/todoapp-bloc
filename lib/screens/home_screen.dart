import 'package:bloc_todoapp/screens/stats_screen.dart';
import 'package:bloc_todoapp/screens/todos_screen.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  static const routeName = '/home';

  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  int pageIndex = 0;
  late PageController pageController;
  List<Widget> screens = [];

  @override
  void initState() {
    pageController = PageController();
    super.initState();
  }

  void onPageChanged(int pageIndex) {
    setState(() {
      this.pageIndex = pageIndex;
    });
  }

  void onTap(int pageIndex) {
    pageController.animateToPage(
      pageIndex,
      curve: Curves.fastLinearToSlowEaseIn,
      duration: const Duration(milliseconds: 400),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: PageView(
        physics: const NeverScrollableScrollPhysics(),
        controller: pageController,
        onPageChanged: onPageChanged,
        children: const <Widget>[
          TodosScreen(),
          StatsScreen(),
        ],
      ),
      bottomNavigationBar: BottomNavigationBar(
        currentIndex: pageIndex,
        onTap: onTap,
        unselectedItemColor: Colors.grey,
        iconSize: 25,
        items: const [
          BottomNavigationBarItem(
            label: 'Home',
            icon: Icon(Icons.list),
          ),
          BottomNavigationBarItem(
            label: 'Test',
            icon: Icon(Icons.trending_up),
          ),
        ],
      ),
    );
  }
}
