import 'dart:async';

import 'package:bloc_todoapp/blocs/todo_bloc.dart';
import 'package:bloc_todoapp/models/todo.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class AddEditScreen extends StatelessWidget {
  const AddEditScreen({
    Key? key,
    this.todo,
  }) : super(key: key);
  final Todo? todo;

  @override
  Widget build(BuildContext context) {
    final TextEditingController task = TextEditingController();
    final TextEditingController note = TextEditingController();
    final TodoBloc bloc = context.watch<TodoBloc>();
    task.text = todo?.task ?? '';
    note.text = todo?.note ?? '';
    return Scaffold(
      appBar: AppBar(
        title: const Text("Add Todo"),
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          autovalidateMode: AutovalidateMode.disabled,
          onWillPop: () {
            return Future(() => true);
          },
          child: ListView(
            children: [
              TextFormField(
                controller: task,
                decoration:
                    const InputDecoration(hintText: 'What needs to be done?'),
                style: Theme.of(context).textTheme.headline5,
              ),
              const SizedBox(
                height: 25,
              ),
              TextFormField(
                controller: note,
                decoration:
                    const InputDecoration(hintText: 'Additional Notes...'),
                maxLines: 10,
                style: Theme.of(context).textTheme.subtitle1,
              )
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: const Icon(Icons.add),
        onPressed: () {
          if (todo != null) {
            bloc.updateTodo(
              task.text,
              note.text,
              todo!.id,
            );
          } else {
            bloc.addTodo(
              Todo(task: task.text, complete: false, note: note.text),
            );
          }

          Navigator.pop(context);
        },
      ),
    );
  }
}
