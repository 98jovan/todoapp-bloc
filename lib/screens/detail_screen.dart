import 'package:bloc_todoapp/blocs/todo_bloc.dart';
import 'package:bloc_todoapp/models/todo.dart';
import 'package:bloc_todoapp/screens/add_edit_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class DetailScreen extends StatelessWidget {
  const DetailScreen({Key? key, required this.todo}) : super(key: key);
  final Todo todo;

  @override
  Widget build(BuildContext context) {
    final bloc = context.read<TodoBloc>();
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text('Todo Details'),
        actions: [
          IconButton(
            icon: const Icon(Icons.delete),
            onPressed: () {
              bloc.removeTodo(todo);
              Navigator.pop(context);
            },
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: StreamBuilder<Object>(
          stream: bloc.todoStream,
          builder: (context, snapshot) {
            return ListView(
              children: [
                Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(right: 8.0),
                      child: Checkbox(
                        value: todo.complete,
                        onChanged: (value) {
                          bloc.toggleTodo(
                            todo,
                            complete: todo.complete,
                          );
                        },
                      ),
                    ),
                    Expanded(
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 8.0,
                              bottom: 16.0,
                            ),
                            child: Text(
                              todo.task,
                              style: Theme.of(context).textTheme.headline5,
                            ),
                          ),
                          Text(
                            todo.note,
                            style: Theme.of(context).textTheme.subtitle1,
                          )
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (context) {
                return AddEditScreen(
                  todo: todo,
                );
              },
            ),
          );
        },
        child: const Icon(Icons.edit),
      ),
    );
  }
}
