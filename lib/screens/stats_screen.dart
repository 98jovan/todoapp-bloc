import 'package:bloc_todoapp/blocs/todo_bloc.dart';
import 'package:bloc_todoapp/widgets/popup_menu_items.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class StatsScreen extends StatelessWidget {
  const StatsScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final bloc = context.watch<TodoBloc>();
    return Scaffold(
      appBar: AppBar(
        title: const Text('Rx Example'),
        actions: [
          AppBarPopupmenu(bloc: bloc),
        ],
      ),
      body: Center(
        child: StreamBuilder<Object>(
          stream: bloc.todoStream,
          builder: (context, snapshot) {
            return Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: Text(
                    'Completed Todos',
                    style: Theme.of(context).textTheme.headline6,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 24.0),
                  child: Text(
                    bloc.todos
                        .where((element) => element.complete)
                        .length
                        .toString(),
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 8.0),
                  child: Text(
                    'Active Todos',
                    style: Theme.of(context).textTheme.headline6,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(bottom: 24.0),
                  child: Text(
                    bloc.todos
                        .where((element) => !element.complete)
                        .length
                        .toString(),
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                )
              ],
            );
          },
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.pushNamed(context, '/add');
        },
        child: const Icon(Icons.add),
      ),
    );
  }
}
