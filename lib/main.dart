import 'package:bloc_todoapp/blocs/todo_bloc.dart';
import 'package:bloc_todoapp/data/todo_repository.dart';
import 'package:bloc_todoapp/screens/add_edit_screen.dart';
import 'package:bloc_todoapp/screens/home_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(
    MultiProvider(
      providers: [
        Provider<TodoBloc>(
          create: (_) =>
              TodoBloc(todoRepository: TodoRepository())..fetchTodos(),
        ),
      ],
      child: const MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData.dark(),
      routes: {
        '/': (BuildContext context) => const HomeScreen(),
        '/add': (BuildContext context) => const AddEditScreen(),
      },
    );
  }
}
