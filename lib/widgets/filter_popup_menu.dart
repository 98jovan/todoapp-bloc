import 'package:bloc_todoapp/blocs/todo_bloc.dart';
import 'package:flutter/material.dart';

class FilterPopUpMenu extends StatelessWidget {
  const FilterPopUpMenu({
    Key? key,
    required this.bloc,
  }) : super(key: key);

  final TodoBloc bloc;

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      icon: const Icon(Icons.filter_list),
      itemBuilder: (context) {
        return [
          PopupMenuItem(
            onTap: () {
              bloc.fetchTodos();
            },
            child: const Text('Show All'),
          ),
          PopupMenuItem(
            onTap: () {
              bloc.toggleActive(complete: false);
            },
            child: const Text('Show Active'),
          ),
          PopupMenuItem(
            onTap: () {
              bloc.toggleActive(complete: true);
            },
            child: const Text('Show Completed'),
          ),
        ];
      },
    );
  }
}
