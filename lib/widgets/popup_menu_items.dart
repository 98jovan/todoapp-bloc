import 'package:bloc_todoapp/blocs/todo_bloc.dart';
import 'package:flutter/material.dart';

class AppBarPopupmenu extends StatelessWidget {
  const AppBarPopupmenu({
    Key? key,
    required this.bloc,
  }) : super(key: key);

  final TodoBloc bloc;

  @override
  Widget build(BuildContext context) {
    return PopupMenuButton(
      icon: const Icon(Icons.more_horiz),
      itemBuilder: (context) {
        return [
          if (bloc.todos.where((element) => element.complete).isNotEmpty)
            PopupMenuItem(
              onTap: () {
                bloc.toggleAll(complete: false);
              },
              child: const Text('Mark all active'),
            )
          else
            PopupMenuItem(
              onTap: () {
                bloc.toggleAll(complete: true);
              },
              child: const Text('Mark all complete'),
            ),
          PopupMenuItem(
            onTap: () {
              bloc.removeAllCompleted();
            },
            child: const Text('Remove completed'),
          ),
        ];
      },
    );
  }
}
