import 'package:bloc_todoapp/models/todo.dart';

List<Todo> todos = [
  Todo(task: "task", complete: true, note: "note"),
  Todo(task: "task", complete: true, note: "note"),
  Todo(task: "task", complete: true, note: "note"),
  Todo(task: "task", complete: true, note: "note")
];

class DataSource {
  List<Todo> getTodos() {
    return todos;
  }

  List<Todo> addTodo(Todo todo) {
    todos.add(
      Todo(task: todo.task, complete: todo.complete, note: todo.note),
    );
    return todos;
  }

  List<Todo> removeTodo(Todo todo) {
    todos.removeWhere((element) => element.id == todo.id);
    return todos;
  }

  List<Todo> toggleTodo(Todo todo, {required bool complete}) {
    final tod = todos.indexWhere((element) => element.id == todo.id);

    todos[tod].complete = !complete;

    return todos;
  }

  List<Todo> toggleAll({required bool complete}) {
    for (final element in todos) {
      element.complete = complete;
    }

    return todos;
  }

  List<Todo> removeAllCompleted() {
    todos.removeWhere((element) => element.complete == true);
    return todos;
  }

  List<Todo> updateTodo(String task, String note, String id) {
    final tod = todos.indexWhere((element) => element.id == id);

    todos[tod].note = note;
    todos[tod].task = task;
    return todos;
  }
}
