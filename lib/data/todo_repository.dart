import 'package:bloc_todoapp/data/data_source.dart';
import 'package:bloc_todoapp/models/todo.dart';

class TodoRepository {
  final DataSource _data = DataSource();

  Future<List<Todo>> getTodos() async {
    return Future.delayed(const Duration(seconds: 4))
        .then((value) => todos = _data.getTodos());
  }

  Future<List<Todo>> addTodo(Todo todo) async {
    return Future.delayed(const Duration(seconds: 4))
        .then((value) => _data.addTodo(todo));
  }

  Future<List<Todo>> updateTodo(String task, String note, String id) async {
    return Future.delayed(const Duration(seconds: 1))
        .then((value) => _data.updateTodo(task, note, id));
  }

  Future<List<Todo>> removeTodo(Todo todo) async {
    return Future.delayed(const Duration(milliseconds: 100))
        .then((value) => _data.removeTodo(todo));
  }

  Future<List<Todo>> toggleTodo(Todo todo, {required bool complete}) async {
    return Future.delayed(const Duration(milliseconds: 100))
        .then((value) => _data.toggleTodo(todo, complete: complete));
  }

  Future<List<Todo>> toggleAll({required bool complete}) async {
    return Future.delayed(const Duration(seconds: 1))
        .then((value) => _data.toggleAll(complete: complete));
  }

  Future<List<Todo>> removeAllCompleted() async {
    return Future.delayed(const Duration(seconds: 2))
        .then((value) => _data.removeAllCompleted());
  }
}
